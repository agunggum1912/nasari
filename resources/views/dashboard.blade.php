<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nasari - Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/plugins/sweetalert2/sweetalert2.min.css') }}">

</head>
<body class="h-100 min-vh-100" style="margin: 0; overflow: hidden;">
    <nav class="navbar bg-body-tertiary">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('/assets/image/logo.png') }}" alt="Bootstrap" class="w-auto" style="height: 50px">
            </a>
        </div>
    </nav>
    <div class="wrapper vh-100">
        <div class="content-wrapper h-100 text-center pt-5">
            <h1>Selamat Datang di Nasari Digital</h1>
        </div>
    </div>
</body>
<script src="{{ asset('/plugins/jquery/jquery.js') }}"></script>
<script src="{{ asset('/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

<script src="{{ asset('/plugins/sweetalert2/sweetalert2.11.min.js') }}"></script>

<script src="https://unpkg.com/feather-icons"></script>
<script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>

<script src="{{ asset('/assets/js/login.js') }}"></script>
<script>
    feather.replace();
</script>
</html>