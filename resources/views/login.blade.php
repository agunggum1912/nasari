<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nasari - Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/plugins/sweetalert2/sweetalert2.min.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body class="h-100 min-vh-100" style="margin: 0; overflow: hidden;">
    <video autoplay muted loop style="position: fixed; top: 50%; left: 50%; min-width: 100%; min-height: 100%; width: auto; height: auto; transform: translate(-50%, -50%); z-index: -1;">
        <source src="{{ asset('/video/background_video_new.mp4') }}" type="video/mp4">
        Your browser does not support the video tag.
    </video>
    <div class="wrapper vh-100">
        <div class="content-wrapper vh-100">
            <div class="row vh-100">
                <div class="offset-md-3 col-md-6 my-auto">
                    <div class="card" style="background-color: #d9d9d9d6">
                        <div class="card-body">
                            <h5 class="text-center">Silahkan Login</h5>

                            <form id="form-login">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="email_prepend" style="background: transparent">
                                        <i data-feather="mail"></i>
                                    </span>
                                    <input 
                                        type="text" 
                                        class="form-control" 
                                        name="email"
                                        id="email"
                                        placeholder="Masukkan Email" 
                                        aria-label="Masukkan Email" 
                                        aria-describedby="email_prepend"
                                    >
                                </div>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="password_prepend" style="background: transparent">
                                        <i data-feather="lock"></i>
                                    </span>
                                    <input 
                                        type="password" 
                                        class="form-control" 
                                        name="password"
                                        id="password"
                                        placeholder="Masukkan Password" 
                                        aria-label="Masukkan Password" 
                                        aria-describedby="password_prepend"
                                    >
                                </div>
                                <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary py-2 px-4">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="{{ asset('/plugins/jquery/jquery.js') }}"></script>
<script src="{{ asset('/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

<script src="{{ asset('/plugins/sweetalert2/sweetalert2.11.min.js') }}"></script>

<script src="https://unpkg.com/feather-icons"></script>
<script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>

<script src="{{ asset('/assets/js/login.js') }}"></script>
<script>
    feather.replace();
</script>
</html>