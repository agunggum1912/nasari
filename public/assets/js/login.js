$(function () {
    "use strict";

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    function csrf() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    $('#form-login').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: "required"
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var data = $(form).serialize();

            csrf()
            $.post(window.location.origin + "/login", data)
                .done(res => {
                    Toast.fire({
                        icon: 'success',
                        title: res.message
                    })
                    setTimeout(function () {
                        window.location.replace("/")
                    }, 1000);
                }).fail(res => {
                    res.status == 500 ?
                        swal.fire('Error', 'Server sibuk, coba beberapa saat kembali.', 'error') :
                        swal.fire('Gagal', res.responseJSON.message, 'warning')
                })
        }
    })
})