<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request) {
        $rule = [
            'email'     => 'required',
            'password'  => 'required'
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails())
            return $this->json400($validate->errors()->first());

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return $this->json200($credentials, "Login berhasil.");
        }

        return $this->json400("Username atau password salah.");
    }
}
